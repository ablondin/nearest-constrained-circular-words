set terminal tikz standalone
set output "benchmark.tex"
set datafile separator ","
set logscale y
set key outside
set xlabel "Length of the word"
set ylabel "Computation time (in seconds)"
set format y "$10^{%T}$"
plot "benchmark.csv" every ::::130 using 1:2 ps 1 pt 7 title "naive"   with points,\
     "benchmark.csv" every ::131   using 1:2 ps 1 pt 7 title "dynamic" with points
