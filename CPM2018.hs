{-|
Module      : CPM2018
Description : Implementation of an algorithm described in a CPM2018 paper
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

Provides data types and functions used to illustrate a dynamic programming
algorithm computing the minimum distance between a configuration and an
admissible word.
 -}
module CPM2018 (
    Conformation, makeConformation, closestWord, compactShow
) where

import Data.List (sort,intercalate,minimumBy,group)
import Data.Ord (comparing)
import qualified Data.Map.Strict as Map
import Text.Printf (printf)

-----------------
-- Basic maths --
-----------------

-- | A Distance type to deal with infinite values
newtype Distance = Distance { distToDouble :: Double }
    deriving (Eq, Ord)

-- | A string representation used when displaying the matrices
instance Show Distance where
    show x | x == infinity = printf " oo"
           | otherwise     = printf format $ distToDouble x
        where format = "%3.0f"

-- | The usual Manhattan distance
--
-- >>> distToDouble $ distance [1,2,3] [3,2,1]
-- 4.0
-- >>> distToDouble $ distance [2,1,3,4] [2,1,3,4]
-- 0.0
distance :: [Int] -> [Int] -> Distance
distance u v = Distance $ fromIntegral $ sum $ map abs $ zipWith (-) u v

-- | Infinity for `Double` type.
infinity = Distance (read "Infinity" :: Double)

-- | Distance behaves like doubles
instance Num Distance where
    Distance d1 + Distance d2 = Distance (d1 + d2)
    Distance d1 * Distance d2 = Distance (d1 * d2)
    abs (Distance d) = Distance (abs d)
    signum (Distance d) = Distance (signum d)
    fromInteger = Distance . fromIntegral
    negate (Distance d) = Distance (negate d)

-----------------------------
-- Combinatorial functions --
-----------------------------

-- | Returns all 2-compositions of a positive integer
--
-- >>> compositions2 5
-- [(0,5),(1,4),(2,3),(3,2),(4,1),(5,0)]
-- >>> compositions2 6
-- [(0,6),(1,5),(2,4),(3,3),(4,2),(5,1),(6,0)]
compositions2 :: Int -> [(Int,Int)]
compositions2 n = [(i,n-i) | i <- [0..n]]

--------------------
-- Word functions --
--------------------

-- | Returns the longest common prefix of two lists
--
-- >>> lcp [2,1,0,3,2,4] [2,1,3,0]
-- [2,1]
-- >>> lcp [1,2,3] [3,2,1]
-- []
lcp :: Eq a => [a] -> [a] -> [a]
lcp _ [] = []
lcp [] _ = []
lcp (x:xs) (y:ys)
    | x == y    = x : lcp xs ys
    | otherwise = []

-- | Returns the longest common suffix of two lists
--
-- >>> lcs [2,1,0,3,2,4] [2,1,3,4]
-- [4]
-- >>> lcs [1,2,3] [3,2,1]
-- []
lcs :: Eq a => [a] -> [a] -> [a]
lcs u v = reverse $ lcp (reverse u) (reverse v)

-- | Checks if two lists are prefix compatible
--
-- Two lists are called /prefix compatible/ if one list is prefix of the other.
--
-- >>> arePrefixCompatible [2,1,0] [2,1]
-- True
-- >>> arePrefixCompatible [] [2,1]
-- True
-- >>> arePrefixCompatible [1,2] [2,1]
-- False
arePrefixCompatible :: Eq a => [a] -> [a] -> Bool
arePrefixCompatible u v = lcp' == u || lcp' == v
    where lcp' = lcp u v

-- | Checks if two lists are suffix compatible
--
-- Two lists are called /suffix compatible/ if one list is suffix of the other.
--
-- >>> areSuffixCompatible [2,1,0] [2,1]
-- False
-- >>> areSuffixCompatible [] [2,1]
-- True
-- >>> areSuffixCompatible [1,2] [2,1]
-- False
areSuffixCompatible :: Eq a => [a] -> [a] -> Bool
areSuffixCompatible u v = lcs' == u || lcs' == v
    where lcs' = lcs u v

------------------
-- Conformation --
------------------

-- | A conformation
--
-- The default constructor should not be used directly. See 'makeConformation'.
data Conformation = Conformation {
    confW  :: [Int], -- The word to treat
    confN  :: Int,   -- The length of the word
    confT  :: Int,   -- The dose
    confL0 :: Int,   -- The minimal length of a closing
    confLt :: Int,   -- The minimal length of an opening
    confL  :: Int    -- The max between L0 and Lt
}

-- | Returns a string representation of a conformation
--
-- >>> makeConformation [1,0,3,2] 2 2 3
-- Conformation{w = [1,0,3,2], n = 4, t = 2, l0 = 2, lt = 3, l = 3}
instance Show Conformation where
    show c = "Conformation{w = " ++ show (confW c) ++ ", n = " ++ show (confN c)
             ++ ", t = " ++ show (confT c) ++ ", l0 = " ++ show (confL0 c) ++
             ", lt = " ++ show (confLt c) ++ ", l = " ++ show (confL c) ++ "}"

-- | Returns a compact string representation of a conformation
--
-- >>> compactShow $ makeConformation [1,0,3,2] 2 2 3
-- "Conformation{n=4, t=2, l0=2, lt=3}"
compactShow :: Conformation -> String
compactShow (Conformation _ n t l0 lt _) = "Conformation{n=" ++ show n
    ++ ", t=" ++ show t ++ ", l0=" ++ show l0 ++ ", lt=" ++ show lt ++ "}"

-- | Builds a conformation for the given parameters
--
-- >>> makeConformation [1,0,3,2,4,2,5] 3 3 5
-- Conformation{w = [1,0,3,2,4,2,5], n = 7, t = 3, l0 = 3, lt = 5, l = 5}
makeConformation :: [Int] -> Int -> Int -> Int -> Conformation
makeConformation w t l0 lt = Conformation {
    confW  = w,
    confN  = length w,
    confT  = t,
    confL0 = l0,
    confLt = lt,
    confL  = max l0 lt
}

-- | Returns a list of admissible prefixes for a given conformation.
--
-- More precisely, the returned list has the property that any admissible word
-- has exactly one prefix among them.
--
-- >>> let conformation = makeConformation [1,0,3,2,4,2,5] 3 3 4
-- >>> fixedPrefixes conformation
-- [[0,3],[0,0,3],[0,0,0,3],[3,0],[3,3,0],[3,3,3,0],[3,3,3,3,0],[0,0,0,0],[3,3,3,3,3]]
fixedPrefixes :: Conformation -> [[Int]]
fixedPrefixes c = [(replicate i 0) ++ [t] | i <- [1..l0]]
               ++ [(replicate i t) ++ [0] | i <- [1..lt]]
               ++ [replicate (l0 + 1) 0, replicate (lt + 1) t]
    where t  = confT c
          l0 = confL0 c
          lt = confLt c

-- | Returns True if the given word is admissible for the given conformation.
--
-- >>> let c = makeConformation [1,0,3,2,4,2,5] 3 3 4
-- >>> isAdmissible c [3,0,0,0,3,3,3,0]
-- False
isAdmissible :: Conformation -> [Int] -> Bool
isAdmissible _ [] = True
isAdmissible (Conformation _ _ t l0 lt _) u
    = (length runLengths <= 2)
      || (and $ zipWith (>=) trimmedRunLengths (cycle [lu1b,lu1]))
    where runLengths = group u
          trimmedRunLengths = map length (init $ tail $ runLengths)
          u1 = head u
          u1b = if u1 == 0 then t else 0
          lu1 = if u1 == 0 then l0 else lt
          lu1b = if u1 == 0 then lt else l0

-- | Returns all admissible words of given length for a given conformation.
--
-- >>> let conformation = makeConformation [1,0,3,2,4,2,5] 3 2 3
-- >>> admissibles conformation 3
-- [[0,0,0],[0,0,3],[0,3,3],[3,0,0],[3,3,0],[3,3,3]]
-- >>> admissibles conformation 4
-- [[0,0,0,0],[0,0,0,3],[0,0,3,3],[0,3,3,3],[3,0,0,0],[3,0,0,3],[3,3,0,0],[3,3,3,0],[3,3,3,3]]
--
-- | Counting them:
--
-- >>> let conformation = makeConformation [1,0,3,2,4,2,5] 3 2 3
-- >>> [length $ admissibles conformation i | i <- [1..15]]
-- [2,4,6,9,14,22,34,52,79,120,183,280,429,657,1005]
admissibles :: Conformation -> Int -> [[Int]]
admissibles (Conformation w n t l0 lt l) k
     = sort $ [replicate i a ++ rest | a <- [0,t],
                                       i <- [1..k],
                                       rest <- admissibles' (bar a)
                                                            (minRun $ bar a)
                                                            (k-i)]
    where minRun 0 = l0
          minRun t = lt
          bar 0 = t
          bar t = 0
          admissibles' :: Int -> Int -> Int -> [[Int]]
          admissibles' a mr k
              | mr > k    = [replicate k a]
              | otherwise = [replicate (mr + j) a ++ admissible
                              | (i,j) <- compositions2 (k - mr),
                                admissible <- admissibles' (bar a)
                                                           (minRun a)
                                                           i]

-- | Returns all circularly admissible words of given lenth for a given
-- conformation.
--
-- >>> let conformation = makeConformation [1,0,3,2,4,2,5] 3 2 3
-- >>> [length $ circularlyAdmissibles conformation i | i <- [1..15]]
-- [0,1,2,2,7,14,23,34,48,69,104,162,254,394,603]
circularlyAdmissibles :: Conformation -> Int -> [[Int]]
circularlyAdmissibles c@(Conformation _ _ _ l0 lt _) k
    = filter circular $ admissibles c k
    where circular [] = True
          circular u
              | u1 == 0 && length u < l0 = False
              | u1 /= 0 && length u < lt = False
              | u1 == uk                 = prefix + suffix >= lu1
              | otherwise                = prefix >= lu1 && suffix >= luk
              where u1 = head u
                    uk = last u
                    lu1 = if u1 == 0 then l0 else lt
                    luk = if uk == 0 then l0 else lt
                    prefix = length $ takeWhile (==u1) u
                    suffix = length $ takeWhile (==uk) (reverse u)

------------
-- Matrix --
------------

-- | A distance matrix
--
-- The constructor should not be used directly. See `initializeMatrix` for more
-- details.
data Matrix a = Matrix {
    matPrefix   :: [Int],
    matConf     :: Conformation,
    matDistance :: (Map.Map ([Int],Int) a)
}

instance Functor Matrix where
    fmap f (Matrix p c d) = Matrix p c (fmap f d)

type DistanceMatrix = Matrix Distance

-- | Returns a string representation of a distance matrix.
--
-- >>> let conformation = makeConformation [2,3,0,2,1] 2 2 3
-- >>> initializeMatrix [0,2] conformation
-- Matrix with fixed prefix [0,2]
-- and Conformation{w = [2,3,0,2,1], n = 5, t = 2, l0 = 2, lt = 3, l = 3}
-- [0,0,0]  -1  -1  -1  -1  -1
-- [0,0,2]  -1  -1  -1  -1  -1
-- [0,2,2]  -1  -1  -1  -1  -1
-- [2,0,0]  -1  -1  -1  -1  -1
-- [2,2,0]  -1  -1  -1  -1  -1
-- [2,2,2]  -1  -1  -1  -1  -1
instance Show a => Show (Matrix a) where
    show (Matrix p c m) = "Matrix with fixed prefix " ++ show p ++ "\n"
        ++ "and " ++ show c ++ "\n"
        ++ (intercalate "\n" $ [lines v | v <- admissibles c (confL c)])
        where lines v = intercalate " " (show v:[show $ (Map.!) m (v,i) | i <- [1..confN c]])

-- | Updates a value in a distance matrix.
--
-- >>> let conformation = makeConformation [2,3,0,2,1] 2 2 3
-- >>> matrixSet ([0,0,2],2) 3 $ initializeMatrix [0,2] conformation
-- Matrix with fixed prefix [0,2]
-- and Conformation{w = [2,3,0,2,1], n = 5, t = 2, l0 = 2, lt = 3, l = 3}
-- [0,0,0]  -1  -1  -1  -1  -1
-- [0,0,2]  -1   3  -1  -1  -1
-- [0,2,2]  -1  -1  -1  -1  -1
-- [2,0,0]  -1  -1  -1  -1  -1
-- [2,2,0]  -1  -1  -1  -1  -1
-- [2,2,2]  -1  -1  -1  -1  -1
matrixSet :: ([Int],Int) -> a -> Matrix a -> Matrix a
matrixSet (v,i) x (Matrix p c m) = Matrix p c m'
    where m' = Map.insert (v,i) x m

-- | Returns the value at given indices in the matrix
--
-- >>> let conformation = makeConformation [2,3,0,2,1] 2 2 3
-- >>> let matrix = initializeMatrix [0,2] conformation
-- >>> matrix
-- Matrix with fixed prefix [0,2]
-- and Conformation{w = [2,3,0,2,1], n = 5, t = 2, l0 = 2, lt = 3, l = 3}
-- [0,0,0]  -1  -1  -1  -1  -1
-- [0,0,2]  -1  -1  -1  -1  -1
-- [0,2,2]  -1  -1  -1  -1  -1
-- [2,0,0]  -1  -1  -1  -1  -1
-- [2,2,0]  -1  -1  -1  -1  -1
-- [2,2,2]  -1  -1  -1  -1  -1
-- >>> distToDouble $ matrixGet ([2,0,0],2) matrix
-- -1.0
matrixGet :: ([Int],Int) -> Matrix a -> a
matrixGet (v,i) (Matrix _ c m) = m Map.! (v,i)

-- | Initializes a distance matrix for a given prefix and given conformation.
--
-- >>> let conformation = makeConformation [0,2,1,3,2] 3 3 5
-- >>> initializeMatrix [0,0,3] conformation
-- Matrix with fixed prefix [0,0,3]    
-- and Conformation{w = [0,2,1,3,2], n = 5, t = 3, l0 = 3, lt = 5, l = 5}
-- [0,0,0,0,0]  -1  -1  -1  -1  -1
-- [0,0,0,0,3]  -1  -1  -1  -1  -1
-- [0,0,0,3,3]  -1  -1  -1  -1  -1
-- [0,0,3,3,3]  -1  -1  -1  -1  -1
-- [0,3,3,3,3]  -1  -1  -1  -1  -1
-- [3,0,0,0,0]  -1  -1  -1  -1  -1
-- [3,0,0,0,3]  -1  -1  -1  -1  -1
-- [3,3,0,0,0]  -1  -1  -1  -1  -1
-- [3,3,3,0,0]  -1  -1  -1  -1  -1
-- [3,3,3,3,0]  -1  -1  -1  -1  -1
-- [3,3,3,3,3]  -1  -1  -1  -1  -1
initializeMatrix :: [Int] -> Conformation -> DistanceMatrix
initializeMatrix p c = Matrix p c m
    where m = Map.fromList $ zip keys (repeat (Distance (-1)))
          keys = [(v,i) | i <- [1..confN c],
                          v <- admissibles c (confL c)]

-- | Initializes the first columns of a distance matrix.
--
-- >>> let conformation = makeConformation [0,2,1,3,2] 3 3 5
-- >>> initializePrefixes $ initializeMatrix [0,0,3] conformation
-- Matrix with fixed prefix [0,0,3]
-- and Conformation{w = [0,2,1,3,2], n = 5, t = 3, l0 = 3, lt = 5, l = 5}
-- [0,0,0,0,0]   0   2  oo  -1  -1
-- [0,0,0,0,3]  oo  oo   4  -1  -1
-- [0,0,0,3,3]  oo  oo  oo  -1  -1
-- [0,0,3,3,3]  oo  oo  oo  -1  -1
-- [0,3,3,3,3]  oo  oo  oo  -1  -1
-- [3,0,0,0,0]   0   2  oo  -1  -1
-- [3,0,0,0,3]  oo  oo   4  -1  -1
-- [3,3,0,0,0]   0   2  oo  -1  -1
-- [3,3,3,0,0]   0   2  oo  -1  -1
-- [3,3,3,3,0]   0  oo  oo  -1  -1
-- [3,3,3,3,3]  oo  oo  oo  -1  -1
initializePrefixes :: DistanceMatrix -> DistanceMatrix
initializePrefixes dm@(Matrix p c m)
    = foldr (uncurry matrixSet) dm values
    where w = confW c
          values = [((v,i), if areSuffixCompatible v (take i p)
                            then distance (take i p) (take i w)
                            else infinity)
                    | i <- [1..length p],
                      v <- admissibles c (confL c)]

-- | Returns the suffixes from which an extension is possible
--
-- >>> let c = makeConformation [4,3,0,1,2,3,2] 2 2 5
-- >>> predSuffixes c [0,2] [0,0,0,2,2] 3
-- [[0,0,0,0,2]]
-- >>> predSuffixes c [2,2] [2,2,2,2,2] 7
-- [[0,2,2,2,2],[2,2,2,2,2]]
predSuffixes :: Conformation -> [Int] -> [Int] -> Int -> [[Int]]
predSuffixes c@(Conformation w n t l0 lt l) p v i
    | hasProblemWithP1  = []
    | hasProblemWithP1B = []
    | i > l             = filter isExtensionAdmissible candidatesIgtL
    | otherwise         = filter isExtensionAdmissible candidatesIltL
    where p1 = head p
          lp1 = if p1 == 0 then l0 else lt
          lp1b = if p1 == 0 then lt else l0
          lengthP = length p
          cp = lp1 - lengthP + 1
          p1b = if p1 == t then 0 else t
          hasProblemWithP1 = nearEndForP1 && wrongSuffixForP1
          hasProblemWithP1B = nearEndForP1B && wrongSuffixForP1B
          nearEndForP1 = n - cp < i && i <= n
          nearEndForP1B = n - cp - lp1b < i && i <= n - cp
          wrongSuffixForP1 = any (/=p1) $ drop (l-i+n-cp) v
          wrongSuffixForP1B = (any (/=p1b) $ drop (l-i+n-cp-lp1b) v) && last v /= p1
          isExtensionAdmissible = isAdmissible c . (++[last v])
          candidatesIgtL = [0:(take (l-1) v), t:(take (l-1) v)]
          candidatesIltL = [replicate (l-i+1) (v!!(l-i)) ++
                            (take (i-1) $ drop (l-i) v)]

-- | Fills a cell in the matrix
--
-- >>> let conformation = makeConformation [0,0,1,1,0,0,0,0,1,1,1,1] 1 3 3
-- >>> let matrix = initializePrefixes $ initializeMatrix [0,1] conformation
-- >>> matrix
-- Matrix with fixed prefix [0,1]
-- and Conformation{w = [0,0,1,1,0,0,0,0,1,1,1,1], n = 12, t = 1, l0 = 3, lt = 3, l = 3}
-- [0,0,0]   0  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [0,0,1]  oo   1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [0,1,1]  oo  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [1,0,0]   0  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [1,1,0]   0  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [1,1,1]  oo  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- >>> fillCell matrix ([0,0,0],3)
-- Matrix with fixed prefix [0,1]
-- and Conformation{w = [0,0,1,1,0,0,0,0,1,1,1,1], n = 12, t = 1, l0 = 3, lt = 3, l = 3}
-- [0,0,0]   0  oo  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [0,0,1]  oo   1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [0,1,1]  oo  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [1,0,0]   0  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [1,1,0]   0  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
-- [1,1,1]  oo  oo  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1
fillCell :: DistanceMatrix -> ([Int],Int) -> DistanceMatrix
fillCell dm@(Matrix p c@(Conformation w n t l0 lt l) m) (v,i)
    = matrixSet (v,i) d dm
    where d = (fromIntegral $ abs ((w!!(i-1)) - (last v))) +
               minimum' [matrixGet (v',i-1) dm | v' <- predSuffixes c p v i]
          lengthP = length p
          p1 = head p
          lp1 = if p1 == 0 then l0 else lt
          cp = lp1 - lengthP + 1
          a = if p1 == t then 0 else 1
          la = if a == 0 then l0 else lt
          minimum' [] = infinity
          minimum' xs = minimum xs

-- | Fills the remaining columns of the matrix
--
-- >>> let conformation = makeConformation [0,0,1,1,0,0,0,0,1,1,1,1] 1 3 3
-- >>> let matrix = initializeMatrix [0,1] conformation
-- >>> fillRemainingColumns $ initializePrefixes matrix
-- Matrix with fixed prefix [0,1]
-- and Conformation{w = [0,0,1,1,0,0,0,0,1,1,1,1], n = 12, t = 1, l0 = 3, lt = 3, l = 3}
-- [0,0,0]   0  oo  oo  oo  oo  oo   1   1   2   3   4   5
-- [0,0,1]  oo   1  oo  oo  oo  oo  oo   2  oo  oo  oo  oo
-- [0,1,1]  oo  oo   1  oo  oo  oo  oo  oo   2  oo  oo  oo
-- [1,0,0]   0  oo  oo  oo  oo   1   2   3   5   7   7   4
-- [1,1,0]   0  oo  oo  oo   1   2   3   4   6   6   3  oo
-- [1,1,1]  oo  oo  oo   1   2   3   4   5   5   2  oo  oo
fillRemainingColumns :: DistanceMatrix -> DistanceMatrix
fillRemainingColumns dm@(Matrix p c _)
    = foldl fillCell dm [(v,i) | i <- [length p+1..confN c],
                                 v <- admissibles c (confL c)]

-- | Returns the distance matrix with filled cells obtained from a conformation
-- for a fixed prefix.
--
-- >>> let conformation = makeConformation [3,1,0,0,2,3,4] 2 5 2          
-- >>> distanceMatrix conformation [2,0,0]
-- Matrix with fixed prefix [2,0,0]             
-- and Conformation{w = [3,1,0,0,2,3,4], n = 7, t = 2, l0 = 5, lt = 2, l = 5}   
-- [0,0,0,0,0]  oo  oo  oo  oo  oo   7  11      
-- [0,0,0,0,2]   1  oo  oo  oo  oo  oo   9      
-- [0,0,0,2,2]   1  oo  oo  oo  oo  oo  oo      
-- [0,0,2,2,0]  oo   2  oo  oo  oo  oo  oo      
-- [0,0,2,2,2]   1  oo  oo  oo  oo  oo  oo      
-- [0,2,2,0,0]  oo  oo   2  oo  oo  oo  oo      
-- [0,2,2,2,0]  oo   2  oo  oo  oo  oo  oo      
-- [0,2,2,2,2]   1  oo  oo  oo  oo  oo  oo      
-- [2,0,0,0,0]  oo  oo  oo  oo   4  oo  oo      
-- [2,2,0,0,0]  oo  oo  oo   2  oo  oo  oo      
-- [2,2,2,0,0]  oo  oo   2  oo  oo  oo  oo      
-- [2,2,2,2,0]  oo   2  oo  oo  oo  oo  oo      
-- [2,2,2,2,2]   1  oo  oo  oo  oo  oo  oo      
distanceMatrix :: Conformation -> [Int] -> DistanceMatrix
distanceMatrix c p = fillRemainingColumns $
                     initializePrefixes $
                     initializeMatrix p c

-- | Returns the pair (v,n) whose corresponding entry is minimal
--
-- >>> let conformation = makeConformation [3,1,0,0,2,3,4] 2 5 2          
-- >>> let matrix = distanceMatrix conformation [2,0,0]
-- >>> indexMin matrix
-- ([0,0,0,0,2],7)
indexMin :: DistanceMatrix -> ([Int],Int)
indexMin dm@(Matrix _ c@(Conformation _ n _ _ _ l) _)
    = minimumBy (comparing ((flip matrixGet) dm)) lastColumn
    where lastColumn = zip (admissibles c l) (repeat n)

-- | Returns the distance of an optimal solution for the given matrix.
--
-- >>> let conformation = makeConformation [3,1,0,0,2,3,4] 2 5 2          
-- >>> let matrix = distanceMatrix conformation [2,0,0]
-- >>> distanceMin matrix == Distance 9
-- True
distanceMin :: DistanceMatrix -> Distance
distanceMin dm = matrixGet (indexMin dm) dm

------------------
-- Backtracking --
------------------

-- | Predecessor states
newtype Predecessors = Predecessors { predToList :: [[Int]] }
    deriving (Eq, Ord)

-- | String representation of predecessors
instance Show Predecessors where
    show (Predecessors ps) = printf format $ show $ map head ps
        where format = "%6.6s"

-- | Returns the predecessors for a given index in a distance matrix.
-- 
-- >>> let conformation = makeConformation [3,1,0,0,2,3,4] 2 5 2
-- >>> let matrix = distanceMatrix conformation [2,0]
-- >>> matrix
-- Matrix with fixed prefix [2,0]               
-- and Conformation{w = [3,1,0,0,2,3,4], n = 7, t = 2, l0 = 5, lt = 2, l = 5}
-- [0,0,0,0,0]  oo  oo  oo  oo  oo   7  oo
-- [0,0,0,0,2]   1  oo  oo  oo  oo  oo   9
-- [0,0,0,2,2]   1  oo  oo  oo  oo  oo  oo
-- [0,0,2,2,0]  oo   2  oo  oo  oo  oo  oo
-- [0,0,2,2,2]   1  oo  oo  oo  oo  oo  oo
-- [0,2,2,0,0]  oo  oo   2  oo  oo  oo  oo
-- [0,2,2,2,0]  oo   2  oo  oo  oo  oo  oo
-- [0,2,2,2,2]   1  oo  oo  oo  oo  oo  oo
-- [2,0,0,0,0]  oo  oo  oo  oo   4  oo  oo
-- [2,2,0,0,0]  oo  oo  oo   2  oo  oo  oo
-- [2,2,2,0,0]  oo  oo   2  oo  oo  oo  oo
-- [2,2,2,2,0]  oo   2  oo  oo  oo  oo  oo
-- [2,2,2,2,2]   1  oo  oo  oo  oo  oo  oo
-- >>> predecessors matrix ([2,2,0,0,0],4)
--  [0,2]
predecessors :: DistanceMatrix -> ([Int],Int) -> Predecessors
predecessors dm@(Matrix _ c _) (v,i)
    = Predecessors $ filter isPredecessor candidates
    where l = confL c
          w = confW c
          t = confT c
          isExtensionAdmissible = isAdmissible c . (++[last v])
          candidates = filter isExtensionAdmissible candidates'
          candidates' = [0:(take (l-1) v), t:(take (l-1) v)]
          prevDist v' = matrixGet (v',i-1) dm
          currentDist = matrixGet (v,i) dm
          diffDist = Distance $ fromIntegral $ abs $ (w!!(i-1)) - (last v)
          isPredecessor v' = (prevDist v') + diffDist == currentDist

-- | Returns a predecessor index for a given index in a distance matrix.
-- 
-- >>> let conformation = makeConformation [3,1,0,0,2,3,4] 2 5 2
-- >>> let matrix = distanceMatrix conformation [2,0]
-- >>> matrix
-- Matrix with fixed prefix [2,0]               
-- and Conformation{w = [3,1,0,0,2,3,4], n = 7, t = 2, l0 = 5, lt = 2, l = 5}
-- [0,0,0,0,0]  oo  oo  oo  oo  oo   7  oo
-- [0,0,0,0,2]   1  oo  oo  oo  oo  oo   9
-- [0,0,0,2,2]   1  oo  oo  oo  oo  oo  oo
-- [0,0,2,2,0]  oo   2  oo  oo  oo  oo  oo
-- [0,0,2,2,2]   1  oo  oo  oo  oo  oo  oo
-- [0,2,2,0,0]  oo  oo   2  oo  oo  oo  oo
-- [0,2,2,2,0]  oo   2  oo  oo  oo  oo  oo
-- [0,2,2,2,2]   1  oo  oo  oo  oo  oo  oo
-- [2,0,0,0,0]  oo  oo  oo  oo   4  oo  oo
-- [2,2,0,0,0]  oo  oo  oo   2  oo  oo  oo
-- [2,2,2,0,0]  oo  oo   2  oo  oo  oo  oo
-- [2,2,2,2,0]  oo   2  oo  oo  oo  oo  oo
-- [2,2,2,2,2]   1  oo  oo  oo  oo  oo  oo
-- >>> predecessor matrix ([0,0,0,0,2],7)
-- ([0,0,0,0,0],6)
predecessor :: DistanceMatrix -> ([Int],Int) -> ([Int],Int)
predecessor dm (v,i)
    | null ps   = error "No predecessor"
    | otherwise = (head ps,i-1)
    where (Predecessors ps) = predecessors dm (v,i)

-- | A matrix of the predecessors
type PredecessorsMatrix = Matrix Predecessors

-- | Creates a matrix of predecessors from a distance Matrix
-- >>> let conformation = makeConformation [3,1,4,2] 2 2 1
-- >>> let matrix = distanceMatrix conformation [0,2]
-- >>> matrix
-- Matrix with fixed prefix [0,2]
-- and Conformation{w = [3,1,4,2], n = 4, t = 2, l0 = 2, lt = 1, l = 2}
-- [0,0]   3  oo  oo  10
-- [0,2]  oo   4  oo  oo
-- [2,0]   3  oo   8   8
-- [2,2]  oo  oo   6  oo
-- >>> makePredecessorsMatrix matrix
-- Matrix with fixed prefix [0,2]
-- and Conformation{w = [3,1,4,2], n = 4, t = 2, l0 = 2, lt = 1, l = 2}
-- [0,0]     []     []  [0,2]    [2]
-- [0,2]     []    [0]    [0]    [0]
-- [2,0]     []  [0,2]    [0]    [2]
-- [2,2]     []  [0,2]    [0]    [0]
makePredecessorsMatrix :: DistanceMatrix -> PredecessorsMatrix
makePredecessorsMatrix dm@(Matrix p c d)
    = foldr (uncurry matrixSet) (Matrix p c Map.empty) values
    where values = [((v,i), predecessors' (v,i))
                    | i <- [1..confN c],
                      v <- admissibles c (confL c)]
          predecessors' (v,i) = if i == 1
                                then Predecessors []
                                else predecessors dm (v,i)

-- | Returns a word whose distance from the conformation is minimal.
--
-- >>> let conformation = makeConformation [3,1,4,2] 2 2 1
-- >>> let matrix = distanceMatrix conformation [0,2]
-- >>> distance (minDistanceWord matrix) [3,1,4,2] == Distance 8
-- True
minDistanceWord :: DistanceMatrix -> [Int]
minDistanceWord dm@(Matrix _ c@(Conformation _ n _ _ _ l) _)
    = (reverse . take n . map (last . fst)) $
      iterate (predecessor dm) (indexMin dm)

----------
-- TikZ --
----------

-- | Returns a TikZ picture string of a distance matrix, with options.
tikzPictureWithOptions :: String -> DistanceMatrix -> String
tikzPictureWithOptions options object
    = "\\begin{tikzpicture}[" ++ options ++ "]\n" ++
        tikz object ++
      "\\end{tikzpicture}"

-- | Returns a TikZ picture string of a distance matrix.
tikzPicture :: DistanceMatrix -> String
tikzPicture = tikzPictureWithOptions ""

-- | Returns a TikZ string of a distance matrix.
tikz :: DistanceMatrix -> String
tikz m@(Matrix p c@(Conformation w n t l0 lt l) d)
    = "  % Lines\n\
          \  \\draw (0.3,0) -- (0.3," ++ show (numSuffixes+1) ++ ");\n\
          \  \\draw (-1," ++ show numSuffixes ++ ") -- ("
              ++ show (n+1) ++ "," ++ show numSuffixes ++ ");\n\
          \  % States\n" ++
          intercalate "\n" (map tikzState (zip [0..] suffixes)) ++ "\n" ++
          "  % Word\n" ++
          intercalate "\n" (map tikzWordLetter (zip [1..] w)) ++ "\n" ++
          "  % Values\n" ++
          intercalate "\n" (map tikzValue values) ++ "\n" ++
          "  % Arrows\n" ++
          intercalate "\n" (map (tikzArrow "black!30") arrows) ++ "\n" ++ 
          "  % Optimal path\n" ++
          intercalate "\n" (map (tikzArrow "black, very thick")
                                (path n optimalV)) ++ "\n"
    where suffixes = reverse $ admissibles c l
          numSuffixes = length suffixes
          zipVI = zip suffixes [1..]
          mapVI = Map.fromList zipVI
          optimalV = minimumBy (comparing (\v -> matrixGet (v,n) m)) suffixes
          values = [(i,j,matrixGet (v,j) m) | (v,i) <- zipVI,
                                                  j <- [1..n]]
          pm = makePredecessorsMatrix m
          arrows = [(i,j,(Map.!) mapVI v')
                          | j <- [2..n],
                        (v,i) <- zipVI,
                           v' <- predToList $ matrixGet (v,j) pm,
                           matrixGet (v,j) m /= infinity]
          path j v
              | null pred = []
              | otherwise = (i,j,i'):path (j-1) v'
              where pred = predToList $ matrixGet (v,j) pm
                    v' = head pred
                    i = (Map.!) mapVI v
                    i' = (Map.!) mapVI v'
          tikzState (i,s) = "  \\node at (-0.5," ++ show i ++ ".5) {"
              ++ concatMap show s ++ "};"
          tikzWordLetter (j,letter) = "  \\node at (" ++ show j ++ ","
              ++ show numSuffixes ++ "+0.5) {" ++ show letter ++ "};"
          tikzValue (i,j,v) = "  \\node (" ++ show j ++ "-" ++ show i ++ ") at ("
              ++ show j ++ "," ++ show i ++ "-0.5) {" ++ show' v ++ "};"
          tikzArrow option (i,j,i') = "  \\draw[" ++ option ++ ", ->] ("
              ++ show j ++ "-" ++ show i
              ++ ") -> (" ++ show (j-1) ++ "-" ++ show i' ++ ");"
          show' v | v == infinity = "$\\infty$"
                  | otherwise     = "$" ++ show v ++ "$"

--------------------------
-- Closest conformation --
--------------------------

-- | Returns a closest word from a given conformation.
-- 
-- >>> let w = take 10 $ cycle [2,3,4,0,5,3,2]
-- >>> let conformation = makeConformation  w 3 4 2
-- >>> let closestNaive = closestWord "naive" conformation
-- >>> let closestDynamic = closestWord "dynamic" conformation
-- >>> distance w closestNaive == distance w closestDynamic
-- True
closestWord :: String -> Conformation -> [Int]
closestWord "naive" c@(Conformation w n _ _ _ _)
    = minimumBy (comparing (distance w)) (circularlyAdmissibles c n)
closestWord "dynamic" c@(Conformation w n _ _ _ _)
    = minDistanceWord minMatrix
    where matrices = [distanceMatrix c p | p <- fixedPrefixes c]
          minMatrix = minimumBy (comparing distanceMin) matrices
