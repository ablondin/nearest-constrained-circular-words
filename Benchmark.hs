import CPM2018
import System.Random
import Data.List (intercalate, zip4)
import Data.List.Split (splitPlaces)
import Criterion.Main

letterSeed = 112155
timeSeed = 91823
l0Seed = 1013945
ltSeed = 42831045

randomList seed minValue maxValue = randomRs (minValue, maxValue) (mkStdGen seed)
randomLetters = randomList letterSeed 0
randomTime    = randomList timeSeed 1
randomL0s     = randomList l0Seed 2
randomLts     = randomList ltSeed 2

randomWords :: [Int] -> Int -> [[Int]]
randomWords lengths maxValue
    = splitPlaces lengths $ randomLetters maxValue

randomConformations :: [Int] -> Int -> Int -> Int -> Int -> Int
                       -> [Conformation]
randomConformations lengths maxValue number maxT maxL0 maxLt
    = [makeConformation w t l0 lt | (w,t,l0,lt) <- zip4
          (randomWords lengths maxValue)
          (randomTime maxT)
          (randomL0s maxL0)
          (randomLts maxLt)
      ]

conformations = randomConformations lengths 20 5 20 5 5
    where lengths = concat [replicate 5 i | i <- [5..35]]

naiveAlgorithm = closestWord "naive"
dynamicAlgorithm = closestWord "dynamic"

main = defaultMain [
    bgroup "naive"   [ bench (compactShow c) $ whnf naiveAlgorithm c   | c <- conformations ],
    bgroup "dynamic" [ bench (compactShow c) $ whnf dynamicAlgorithm c | c <- conformations ]
    ]
