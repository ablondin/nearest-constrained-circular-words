# Nearest constrained circular word

This repository contains Haskell code used to implement an algorithm described
in the article titled "Nearest constrained circular word", submitted to the
[29th Annual Symposium on Combinatorial Pattern Matching (CPM
2018)](http://cpm2018.sdu.edu.cn/), by Guillaume Blin, Alexandre Blondin Massé,
Sylvie Hamel, Marie Gasparoux and Élise Vandomme.

## Author

Alexandre Blondin Massé, in consultation with Sylvie Hamel and Élise Vandomme.

## Dependencies

- [The Glasgow Haskell Compiler](https://www.haskell.org/ghc/);
- [Haddock](https://www.haskell.org/haddock/) (optional), to generate the
  documentation;
- [Doctest](https://github.com/sol/doctest) (optional), to test the example in
  the docstrings.
- [Criterion](http://www.serpentine.com/criterion/) (optional), a Haskell
  module that makes benchmarking easier.

## Usage

First, launch the interpreter with the command
```sh
ghci
```
The [CPM2018.hs](CPM2018.hs) module can be loaded directly with
```haskell
Prelude> :load CPM2018.hs
```
To create a conformation:
```haskell
*CPM2018> let conformation = makeConformation [2,1,4,3,0,5,2,3,1,4] 3 4 2
*CPM2018> conformation
Conformation{w = [2,1,4,3,0,5,2,3,1,4], n = 10, t = 3, l0 = 4, lt = 2, l = 4}
```
A closest solution can be obtained either by a naive strategy
```haskell
*CPM2018> closestWord "naive" conformation
[3,3,3,3,3,3,3,3,3,3]
```
or our dynamic programming algorithm
```haskell
*CPM2018> closestWord "dynamic" conformation
[3,3,3,3,3,3,3,3,3,3]
```
The matrices produced by the dynamic programming algorithm can be easily
viewed. For instance, with the prefix `[0,3]`, it can be done with
```haskell
*CPM2018> distanceMatrix conformation [0,3]
Matrix with fixed prefix [0,3]
and Conformation{w = [2,1,4,3,0,5,2,3,1,4], n = 10, t = 3, l0 = 4, lt = 2, l = 4}
[0,0,0,0]   2  oo  oo  oo  oo  oo  15  15  16  20
[0,0,0,3]  oo   4  oo  oo  oo  oo  oo  oo  oo  oo
[0,0,3,3]  oo  oo   5  oo  oo  oo  oo  oo  oo  oo
[0,3,3,0]   2  oo  oo   8  oo  oo  oo  oo  oo  oo
[0,3,3,3]  oo  oo  oo   5  oo  oo  oo  oo  oo  oo
[3,0,0,0]   2  oo  oo  oo  oo  13  12  18  16  19
[3,3,0,0]   2  oo  oo  oo   8  10  15  15  15  oo
[3,3,3,0]   2  oo  oo  oo   5  13  12  14  oo  oo
[3,3,3,3]  oo  oo  oo  oo   8  10  11  oo  oo  oo
```
where `oo` denotes $`\infty`$.

To generate the documentation, simply type
```sh
make doc
```

It is also possible to test the examples in the docstrings by typing
```sh
make test
```

## Benchmarking

Using Haskell's [Criterion](http://www.serpentine.com/criterion/), we produced
a small benchmark showing that the dynamic programming algorithm is more
efficient than the naive approach:

![](stats/benchmark.png)

## License

All code contained in this repository is licensed under the [GNU General Public
License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).  All documents and
resources are subjected to the Creative Commons Attribution 4.0 International
License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.
