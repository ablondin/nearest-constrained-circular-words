DOC_DIR = doc/
BENCH_DIR = bench/
HS_FILES = CPM2018.hs
BENCH_FILE = Benchmark

.PHONY: all bench clean doc docdir test

all: doc test

test:
	doctest $(HS_FILES)

docdir:
	mkdir -p $(DOC_DIR)

benchdir:
	mkdir -p $(BENCH_DIR)

doc: docdir $(HS_FILES)
	haddock -o $(DOC_DIR) --html $(HS_FILES)

bench: benchdir
	ghc -O --make $(BENCH_FILE)
	caffeinate ./$(BENCH_FILE) --csv $(BENCH_DIR)/benchmark.csv
	caffeinate ./$(BENCH_FILE) --output $(BENCH_DIR)/benchmark.html
	cp -r $(BENCH_DIR) ~/Dropbox/CPM2018/

clean:
	rm -f *.o *.hi
	rm -rf $(BENCH_DIR)
